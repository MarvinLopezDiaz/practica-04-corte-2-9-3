package com.example.practica04corte29_3;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


public class Adaptador extends RecyclerView.Adapter<Adaptador.ViewHolder> implements View.OnClickListener{
    protected ArrayList<Alumno> listAlumnos;
    private View.OnClickListener listener;
    private LayoutInflater inflater;
    private  Context context;

    public Adaptador(ArrayList<Alumno> listAlumnos, Context context){
        this.listAlumnos = listAlumnos;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @NotNull
    @Override
    public Adaptador.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.alumnos_items, null, false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Adaptador.ViewHolder holder, int position) {
        Alumno alumno = listAlumnos.get(position);
        holder.txtMatricula.setText(alumno.getMatricula());
        holder.txtNombre.setText(alumno.getNombre());
        holder.txtCarrea.setText(alumno.getCarrera());

        holder.idImagen.setImageResource(alumno.getImg());
    }

    @Override
    public int getItemCount() { return listAlumnos.size(); }

    public void setOnClickListener(View.OnClickListener listener){ this.listener = listener;}

    @Override
    public void onClick(View view) { if(listener != null) listener.onClick(view); }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private LayoutInflater inflater;
        private TextView txtNombre;
        private TextView txtMatricula;
        private TextView txtCarrea;

        private ImageView idImagen;

        public ViewHolder(@NonNull @NotNull View itemView){
            super(itemView);
            txtNombre = (TextView) itemView.findViewById(R.id.txtNombre);
            txtMatricula = (TextView) itemView.findViewById(R.id.txtMatricula);
            txtCarrea = (TextView) itemView.findViewById(R.id.txtCarrea);

            idImagen = (ImageView) itemView.findViewById((R.id.foto));
        }
    }
}
