package com.example.practica04corte29_3;

import android.app.Application;
import android.util.Log;

import java.util.ArrayList;

public class Aplicacion extends Application {
    static ArrayList<Alumno> alumnos;
    private static  Adaptador adaptador;

    public ArrayList<Alumno> getAlumnos(){return alumnos;}
    public Adaptador getAdaptador(){return adaptador;}

    public void onCreate(){
        super.onCreate();
        alumnos = Alumno.llenarAlumnos();
        adaptador = new Adaptador(alumnos, this);
        Log.d("","onCreate: tamaño array list" + alumnos.size());
    }
}
